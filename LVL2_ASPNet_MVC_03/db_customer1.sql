create database db_customer1
use db_customer1

create table tbl_customer1
(
	id int identity primary key,
	name char(100),
	phone_number char(15),
	address varchar(400),
	email char(100),
	description varchar(400)
)

select * from tbl_customer1