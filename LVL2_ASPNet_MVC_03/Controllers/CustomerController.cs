﻿using LVL2_ASPNet_MVC_03.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LVL2_ASPNet_MVC_03.Controllers
{
    public class CustomerController : Controller
    {
        db_customer1Entities db = new db_customer1Entities();
        // GET: Customer
        public ActionResult Index()
        {
            return View(db.tbl_customer1.ToList());
        }

        // GET: Customer/Details/5
        public ActionResult Details(int id)
        {
            return View(db.tbl_customer1.Where(x => x.id == id).FirstOrDefault());
        }

        // GET: Customer/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Customer/Create
        [HttpPost]
        public ActionResult Create(tbl_customer1 customer) // ini model
        {
            /*
            try
            {
                int a = 10;
                int b = 2; 
                int c = a / b;
            }
            catch (Exception msg)
            {

                ViewBag.Message = msg;
            }*/

            using (var transaction = db.Database.BeginTransaction()) 
            {
                try
                {
                    // TODO: Add insert logic here
                    // Insert new customer [Proses 1]
                    db.tbl_customer1.Add(customer); //add modelnya
                    db.SaveChanges();

                    // [Proses 3]
                    int a = 10;
                    int b = 0;
                    int c = a / b;

                    //update address [Proses 2]
                    tbl_customer1 edit = db.tbl_customer1.Where(x => x.id == 1).FirstOrDefault();
                    edit.address = "Sukabumi";
                    db.Entry(edit).State = EntityState.Modified;
                    db.SaveChanges();
                    transaction.Commit();
                    return RedirectToAction("Index");
                }
                catch (Exception msg)
                {
                    transaction.Rollback();
                    ViewBag.Message = msg;
                    string error_action = "create customer";
                    string error_section = "create customer";
                    string user_login = "budi123";
                    var error_msg = msg;

                    // put data into auditTrail table
                    tbl_auditTrail err = new tbl_auditTrail
                    {
                        error_msg = msg.ToString(),
                        user_login = user_login,
                        error_date = DateTime.Now,
                        error_section = error_section,
                        error_action = error_action;
                    };
                    db.Entry(err).State = EntityState.Added;
                    db.SaveChanges();
                    return View();
                }
            }
                
            // return View();
        }

        // GET: Customer/Edit/5
        public ActionResult Edit(int id)
        {
            return View(db.tbl_customer1.Where(x => x.id == id).FirstOrDefault());
        }

        // POST: Customer/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, tbl_customer1 customer)
        {
            try
            {
                // TODO: Add update logic here
                db.Entry(customer).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Customer/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Customer/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, tbl_customer1 customer)
        {
            try
            {
                // TODO: Add delete logic here
                customer = db.tbl_customer1.Where(x => x.id == id).FirstOrDefault();
                db.tbl_customer1.Remove(customer);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}